import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";

const config = {
  entry: [path.join(__dirname, "../", "src", "index")],

  output: {
    path: path.join(__dirname, "../", "dist"),
    filename: "js/[name]_[hash].js",
    publicPath: "/"
  },

  devServer: {
    contentBase: path.join(__dirname, "public"),
    watchContentBase: true,
    publicPath: "/dist/",
    historyApiFallback: true
  },
  resolve: {
    alias: {
      "@components": path.resolve(__dirname, "../", "src", "components"),
      "@pages": path.resolve(__dirname, "../", "src", "Pages"),
      "@root": path.resolve(__dirname, "../", "src", ""),
      "@services": path.resolve(__dirname, "../", "src", "services"),
      "@styles": path.resolve(__dirname, "../", "src", "styles")
    }
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/, /src\/styles\/vendor/],
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(jpe?g|gif|ttf|eot|svg|png|woff(2)?)(\?[a-z0-9=&.]*)?$/,
        use: {
          loader: "file-loader"
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: "./index.html"
    })
  ]
};

export default config;
