# Marvel Test

> Marvel Test with React, Jest, ESLint, Env Config and more

## Prerequisites

Make sure you have installed all of the following prerequisites on your development machine:

- Git - [Download & Install Git](https://git-scm.com/downloads).

- Node.js - [Download & Install Node.js](https://nodejs.org/en/download/)

## Cloning The GitHub Repository

```bash
$ git clone https://digoribeiro@bitbucket.org/digoribeiro/marve-project.git
```

To install the dependencies, run this in the application folder from the command-line:

```bash
$ cd marve-project && npm install
```

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:8000](http://localhost:8000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## Stack

- [React](https://reactjs.org/) - It's an open-source JavaScript library created by Facebook. It's used to help developers build user interfaces using components.

- [Eslint](http://eslint.org/) - It's an open source JavaScript linting utility. It's used to help keep the code within certain style guidelines. In this project the Airbnb style guide is used.

- [Jest](https://facebook.github.io/jest/) - It's an open-source testing library for JavaScript code.

- [Prettier](https://prettier.io/) - It's a JavaScript code formatter. It gets rid of original styling and offers a consistent style by parsing the code and re-printing it with its rules.

- [Prop Types](https://www.npmjs.com/package/prop-types/) - Runtime type checking for React props and similar objects.

- [Styled Components](https://styled-components.com/) - It's a CSS-in-JS styling framework. It uses tagged template literals in JavaScript plus CSS to allow developers to style React components.

As you can see, we use a lot of great tools and if you want to see more, take a look at our [package.json](package.json).

## Code Standarts

This project uses [eslint](http://eslint.org/) and [.editorconfig](http://editorconfig.org/) is defined to have indent_size of **2 spaces**.
