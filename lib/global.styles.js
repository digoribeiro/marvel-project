"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledComponents = require("styled-components");

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  * {\n    box-sizing: border-box;\n    padding: 0;\n    margin: 0;\n    outline: 0;\n  }\n\n  body {\n    font-family: -apple-system, Ubuntu, sans-serif;\n    font-size: 14px;\n    font-weight: 400;\n    line-height: 1.5;\n    background: #f0f0f5;\n    -webkit-font-smoothing: antialiased;\n  }\n\n  -moz-user-input, button, textarea {\n    font-family: -apple-system, Ubuntu, sans-serif;\n    font-size: 18px;\n    font-weight: 400;\n  }\n\n  button {\n    cursor: point;\n  }\n\n  .container {\n    flex-grow: 1;\n    margin: 0 auto;\n    position: relative;\n    width: auto;\n  }\n\n  @media screen and (min-width: 1024px) {\n    .container {\n        max-width: 960px;\n    }\n  }\n\n  @media screen and (min-width: 1216px) {\n    .container {\n        max-width: 1152px;\n    }\n  }\n\n  @media screen and (min-width: 1408px) {\n    .container {\n        max-width: 1344px;\n    }\n  }\n\n  .columns {\n    display: flex;\n  }\n\n  .columns-wrap {\n    flex-wrap: wrap;\n  }\n\n  .columns-justify-content {\n    justify-content: space-between;\n  }\n\n  .column {\n    margin-left:  10px;\n  }\n\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var GlobalStyle = (0, _styledComponents.createGlobalStyle)(_templateObject());
var _default = GlobalStyle;
exports["default"] = _default;