"use strict";

var _react = _interopRequireDefault(require("react"));

var _ = _interopRequireDefault(require("."));

var _react2 = require("@testing-library/react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

describe("Header", function () {
  it("Should render correctly", function () {
    var _render = (0, _react2.render)( /*#__PURE__*/_react["default"].createElement(_["default"], null)),
        container = _render.container;

    expect(container).toMatchSnapshot();
  });
});