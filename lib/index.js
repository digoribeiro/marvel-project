"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

var _reactRouterDom = require("react-router-dom");

var _history = _interopRequireDefault(require("@root/history"));

var _App = _interopRequireDefault(require("@root/App"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

(0, _reactDom.render)( /*#__PURE__*/_react["default"].createElement(_reactRouterDom.BrowserRouter, {
  history: _history["default"]
}, /*#__PURE__*/_react["default"].createElement(_App["default"], null)), document.getElementById("root"));