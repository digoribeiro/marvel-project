import mockAxios from 'axios';
import getCharacters from './getCharacters';

const privateKey = '323d58c7f9a0ec779f763666286587f132d97556';
const publicKey = '1a78af271c781a0c0fe83bb82edd1135';
const url = '/characters';

const data = {
  data: {
    offset: 0,
    limit: 20,
    total: 1493,
    count: 20,
    results: [
      {
        0: {
          id: 1011334,
          name: '3-D Man',
          description: '',
          modified: '2014-04-29T14:18:17-0400'
        },
        1: {
          id: 1017100,
          name: 'A-Bomb (HAS)',
          description:
            "Rick Jones has been Hulk's best bud since day one,…ses it like a giant bowling ball of destruction! ",
          modified: '2013-09-18T15:54:04-0400'
        }
      }
    ]
  }
};
describe('GetCharacters', () => {
  it('fetches successfully data from an API', async () => {
    mockAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: {
          data
        }
      })
    );
    const response = await getCharacters(privateKey, publicKey, url);

    expect(response.data).toEqual(data);
    expect(mockAxios.get).toBeCalledTimes(1);
  });
});
