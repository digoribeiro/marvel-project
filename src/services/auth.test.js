import isAuthenticated from './auth';

describe('Auth', () => {
  beforeEach(() => {
    global.localStorage.clear();
    global.localStorage.setItem('publicKey', '1a78af271c781a0c0fe83bb82edd1135');
    global.localStorage.setItem('privateKey', '1a78af271c781a0c0fe83bb82edd1135');
  });

  it('fetches successfully auth', async () => {
    const publicKey = global.localStorage.getItem('publicKey');
    const privateKey = global.localStorage.getItem('privateKey');

    const simulateAuth = isAuthenticated(publicKey && privateKey);
    expect(simulateAuth).toEqual(true);
  });

  it('fetches error auth', async () => {
    const publicKey = global.localStorage.getItem('publicKey');
    const privateKey = global.localStorage.getItem('privateKey');
    global.localStorage.removeItem('publicKey');

    const simulateAuth = isAuthenticated(publicKey && privateKey);
    expect(simulateAuth).toEqual(false);
  });
});
