const isAuthenticated = () => {
  const publicKey = global.localStorage.getItem('publicKey');
  const privateKey = global.localStorage.getItem('privateKey');

  if (publicKey && privateKey) {
    return true;
  }

  return false;
};

export default isAuthenticated;
