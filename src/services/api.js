import axios from 'axios';

const cors = 'https://cors-anywhere.herokuapp.com/';
const uri = 'https://gateway.marvel.com:443/v1/public';

const api = axios.create({
  baseURL: `${cors}${uri}`
});

export default api;
