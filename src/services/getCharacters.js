import CryptoJS from 'crypto-js';
import api from './api';

export const getCharacters = async (privKey, publicKey, url = 'characters') => {
  const ts = new Date().getTime();
  const hash = CryptoJS.MD5(ts + privKey + publicKey).toString();

  const data = await api.get(url, {
    params: {
      limit: 100,
      ts,
      apikey: publicKey,
      hash
    }
  });
  return data.data;
};

export default getCharacters;
