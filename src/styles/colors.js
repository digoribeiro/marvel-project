export default {
  primary: '#3273dc',
  secundary: '#209cee',

  greyDark: '#363636',
  greyLight: '#dbdbdb',

  black: '#000000',
  white: '#FFFFFF',
  light: '#f5f5f5',
  dark: '#4a4a4a',

  info: '#ffdd57',
  danger: '#ff3860',
  success: '#48c774',

  transparent: 'rgba(0, 0, 0, 0)'
};
