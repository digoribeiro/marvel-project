import React from 'react';
import { render } from '@testing-library/react';
import Notification from '.';

describe('Notification', () => {
  it('Should render correctly type of success', () => {
    const { container } = render(<Notification text="Message" type="success" />);
    expect(container).toMatchSnapshot();
  });

  it('Should render correctly type of error', () => {
    const { container } = render(<Notification text="Message" type="error" />);
    expect(container).toMatchSnapshot();
  });
});
