import styled from 'styled-components';
import { colors } from '@styles';

const Wrapper = styled.div`
  width: 100%;
  background: ${props => (props.type === 'error' ? colors.danger : colors.success)};
  color: ${colors.white};
  display: flex;
  border-radius: 4px;
  padding: 1.25rem 2.5rem 1.25rem 1.5rem;
  position: relative;
`;

export default Wrapper;
