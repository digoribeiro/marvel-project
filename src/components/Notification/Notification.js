import React from 'react';
import PropTypes from 'prop-types';
import Wrapper from './Notification.styles';

const Notification = ({ type, text }) => {
  return <Wrapper type={type}>{text}</Wrapper>;
};

Notification.propTypes = {
  text: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['error', 'success']).isRequired
};

export default Notification;
