import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import Header from '.';

describe('Header', () => {
  beforeEach(() => {
    jest.mock('react-router-dom', () => ({
      useHistory: () => ({
        push: jest.fn()
      })
    }));
  });

  const history = createMemoryHistory();
  const route = '/Home';
  history.push(route);

  it('Should render correctly', () => {
    const { wrapper } = render(
      <Router history={history}>
        <Header />
      </Router>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('Should handle click logout', () => {
    const handleLogout = jest.fn();

    const wrapper = mount(
      <Router history={history}>
        <Header onClick={handleLogout} />
      </Router>
    );

    expect(wrapper.find('.logout')).toHaveLength(1);
  });
});
