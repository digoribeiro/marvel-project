import styled from 'styled-components';
import { colors } from '@styles';

export const Wrapper = styled.div`
  position: relative;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  a {
    font-size: 16px;
    color: ${colors.primary};
    text-decoration: none;
  }

  @media screen and (max-width: 767px) {
    flex-direction: column;
    justify-content: center;
  }
`;

export const Logo = styled.h1`
  color: ${colors.greyDark};
  font-size: 28px;
`;
