import React from 'react';
import { useHistory } from 'react-router-dom';
import { Wrapper, Logo } from './Header.styles';

const Header = () => {
  const history = useHistory();

  const handleLogout = () => {
    global.localStorage.clear();
    history.push('/home');
  };

  return (
    <header>
      <div className="container">
        <Wrapper>
          <Logo>Marvel Comics API</Logo>
          <a className="logout" href="#" onClick={() => handleLogout()}>
            SAIR
          </a>
        </Wrapper>
      </div>
    </header>
  );
};

export default Header;
