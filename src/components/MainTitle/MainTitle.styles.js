import styled from 'styled-components';
import { colors } from '@styles';

const Title = styled.h1`
  font-size: 3rem;
  text-align: center;
  color: ${colors.dark};
`;

export default Title;
