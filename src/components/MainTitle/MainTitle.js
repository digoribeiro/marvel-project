import React from 'react';
import PropTypes from 'prop-types';
import Title from './MainTitle.styles';

const MainTitle = ({ text }) => <Title>{text}</Title>;

MainTitle.propTypes = {
  text: PropTypes.string.isRequired
};

export default MainTitle;
