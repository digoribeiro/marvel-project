import React from 'react';
import { render } from '@testing-library/react';
import MainTitle from '.';

describe('MainTitle', () => {
  it('Should render correctly', () => {
    const { container } = render(<MainTitle text="Fake" />);
    expect(container).toMatchSnapshot();
  });
});
