import styled from 'styled-components';
import { colors } from '@styles';

export const Wrapper = styled.div`
  margin: 10px auto;
  position: relative;
`;

export const Label = styled.label`
  font-size: 16px;
  font-weight: 600;
  display: block;
  margin-bottom: 5px;
`;

export const InputText = styled.input`
  max-width: 100%;
  width: 100%;
  background-color: ${colors.white};
  border-radius: 4px;
  color: ${colors.greyDark};
  border: 1px solid ${colors.greyLight};
  font-size: 1rem;
  height: 40px;
  line-height: 1.5;
  padding-bottom: calc(0.5em - 1px);
  padding-left: calc(0.75em - 1px);
  padding-right: calc(0.75em - 1px);
  padding-top: calc(0.5em - 1px);
  position: relative;
`;

export const Icon = styled.i`
  position: absolute;
  top: 40px;
  right: 20px;
  color: ${colors.greyLight};
`;
