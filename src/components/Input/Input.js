import React from 'react';
import PropTypes from 'prop-types';
import { Label, InputText, Wrapper, Icon } from './Input.styles';

const Input = ({
  label,
  id,
  onChange,
  name,
  type,
  placeholder,
  icon,
  required,
  value,
  ...rest
}) => {
  return (
    <Wrapper {...rest}>
      <Label htmlFor={id}>{label}</Label>
      <InputText
        type={type}
        onChange={onChange}
        name={name}
        id={id}
        placeholder={placeholder}
        required={required}
        value={value}
      />
      {icon && <Icon className={`fas fa-${icon}`} />}
    </Wrapper>
  );
};

Input.defaultProps = {
  required: false,
  type: 'text',
  onChange: null,
  value: '',
  icon: ''
};

Input.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  icon: PropTypes.string,
  required: PropTypes.bool,
  value: PropTypes.string
};

export default Input;
