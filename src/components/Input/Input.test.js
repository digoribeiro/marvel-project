import React from 'react';
import { render } from '@testing-library/react';
import Input from '.';

describe('Input', () => {
  it('Should render correctly', () => {
    const { container } = render(
      <Input label="Fake" id="fake" name="fake" icon="allergies" placeholder="Fake placeholder" />
    );
    expect(container).toMatchSnapshot();
  });

  it('Should render required', () => {
    const { container } = render(
      <Input
        label="Fake"
        id="fake"
        name="fake"
        icon="allergies"
        placeholder="Fake placeholder"
        required
      />
    );
    expect(container).toMatchSnapshot();
  });

  it('Should render no icon props', () => {
    const { container } = render(
      <Input label="Fake" id="fake" name="fake" placeholder="Fake placeholder" />
    );
    expect(container).toMatchSnapshot();
  });

  it('Should render type password', () => {
    const { container } = render(
      <Input type="password" label="Fake" id="fake" name="fake" placeholder="Fake placeholder" />
    );
    expect(container).toMatchSnapshot();
  });
});
