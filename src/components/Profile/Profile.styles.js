import styled from 'styled-components';
import { colors } from '@styles';

const textStyle = `
  color: ${colors.greyDark};
  line-height: 1.125;
  font-weight: 300;
`;

export const Wrapper = styled.div`
  width: 80%;
  margin: 40px auto;
  display: flex;
  align-items: center;

  @media screen and (max-width: 767px) {
    flex-direction: column;
  }
`;

export const WrapperText = styled.div`
  background: ${colors.white};
  width: 100%;
  padding: 20px 10px;
  min-height: 145px;
  display: flex;
  flex-direction: column;
  justify-content: center;

  span {
    font-size: 22px;
    font-weight: 600;
  }
`;

export const Name = styled.h2`
  font-size: 2rem;
  ${textStyle}
`;

export const TextInfo = styled.h3`
  font-size: 18px;
  margin: 10px 0;
  ${textStyle}
`;

export const Description = styled.p`
  font-size: 14px;
  ${textStyle}
`;

export const Avatar = styled.img`
  width: 130px;
  height: auto;
  margin-right: 10px;

  @media screen and (max-width: 767px) {
    width: 80%;
  }
`;

export const WrapperInfo = styled.div`
  display: flex;
  width: 80%;
  justify-content: space-between;
`;
