import React from 'react';
import { render } from '@testing-library/react';
import Profile from '.';

describe('Profile', () => {
  it('Should render correctly', () => {
    const { container } = render(
      <Profile
        name="Rodrigo"
        description="Software Developer"
        avatar="https://placekitten.com/g/200/300"
      />
    );
    expect(container).toMatchSnapshot();
  });

  it('Should render with title and edition', () => {
    const { container } = render(
      <Profile
        name="Rodrigo"
        description="Software Developer"
        avatar="https://placekitten.com/g/200/300"
        title="Olá mundo!"
        edition="41"
      />
    );
    expect(container).toMatchSnapshot();
  });
});
