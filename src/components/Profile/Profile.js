import React from 'react';
import PropTypes from 'prop-types';

import {
  Wrapper,
  WrapperText,
  WrapperInfo,
  Name,
  Description,
  Avatar,
  TextInfo
} from './Profile.styles';

const Profile = ({ name, description, avatar, edition, title }) => {
  return (
    <Wrapper>
      <Avatar src={avatar} alt={`Image of: ${name}`} />
      <WrapperText>
        <Name>
          <span>Nome: </span>
          {name}
        </Name>
        {title || edition ? (
          <WrapperInfo>
            {title && (
              <TextInfo>
                <span>Título: </span>
                {title}
              </TextInfo>
            )}
            {edition && (
              <TextInfo>
                <span>Edição: </span>
                {edition}
              </TextInfo>
            )}
          </WrapperInfo>
        ) : (
          ''
        )}
        {description && (
          <Description>
            <span>Descrição: </span>
            {description}
          </Description>
        )}
      </WrapperText>
    </Wrapper>
  );
};

Profile.defaultProps = {
  edition: null,
  title: null
};

Profile.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  edition: PropTypes.number,
  title: PropTypes.string
};

export default Profile;
