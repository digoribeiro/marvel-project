import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';

import Link from '.';

describe('Link', () => {
  it('Should render correctly', () => {
    const { container } = render(
      <Router>
        <Link link="/home" text="Link" onClick={() => console.log('Olá mundo!')} />
      </Router>
    );
    expect(container).toMatchSnapshot();
  });
});
