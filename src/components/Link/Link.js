import React from 'react';
import { Link as LinkRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import Wrapper from './Link.styles';

const Link = ({ text, link, onClick, sticky, ...rest }) => (
  <Wrapper {...rest}>
    <LinkRouter to={link} onClick={onClick}>
      {text}
    </LinkRouter>
  </Wrapper>
);

Link.defaultProps = {
  link: '#',
  onClick: null
};

Link.propTypes = {
  text: PropTypes.string.isRequired,
  link: PropTypes.string,
  onClick: PropTypes.func
};

export default Link;
