import styled from 'styled-components';
import { colors } from '@styles';

const Wrapper = styled.div`
  a {
    font-size: 16px;
    color: ${colors.primary};
    text-decoration: none;
    cursor: pointer;
    transition: all 0.4s ease;

    :hover {
      color: ${colors.black};
    }
  }
`;

export default Wrapper;
