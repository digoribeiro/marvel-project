import React from 'react';
import Wrapper from './Loading.styles';

const Loading = () => {
  return (
    <Wrapper>
      Loading
      <i className="fas fa-spinner fa-pulse" />
    </Wrapper>
  );
};

export default Loading;
