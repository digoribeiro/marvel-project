import styled from 'styled-components';
import { colors } from '@styles';

const Wrapper = styled.div`
  color: ${colors.greyDark};
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
  font-size: 32px;
  position: absolute;
  background: #f0f0f5;
  z-index: 1;
  transition: all 0.4s ease;

  i {
    margin-left: 20px;
  }
`;

export default Wrapper;
