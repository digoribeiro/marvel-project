import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper, Icon } from './Button.styles';

const Button = ({ text, onClick, icon, type, ...rest }) => (
  <Wrapper type={type} onClick={onClick} {...rest}>
    {text}
    {icon && <Icon className={`fas fa-${icon}`} />}
  </Wrapper>
);

Button.defaultProps = {
  type: 'submit',
  onClick: null,
  icon: 'arrow-right'
};

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.string,
  icon: PropTypes.string,
  type: PropTypes.string
};

export default Button;
