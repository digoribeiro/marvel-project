import styled from 'styled-components';
import { colors } from '@styles';

export const Wrapper = styled.button`
  width: 100%;
  margin: 10px 0;
  cursor: pointer;
  position: relative;
  background-color: ${colors.primary};
  color: ${colors.white};
  justify-content: center;
  padding-bottom: calc(0.5em - 1px);
  padding-left: 1em;
  padding-right: 1em;
  padding-top: calc(0.5em - 1px);
  text-align: center;
  white-space: nowrap;
  border-width: 1px;
  border-radius: 4px;
  height: 40px;
  transition: all 0.4s ease;

  &:hover {
    opacity: 0.8;
  }
`;

export const Icon = styled.i`
  right: 20px;
  margin-left: 10px;
  color: ${colors.white};
`;
