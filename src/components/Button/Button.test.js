import React from 'react';
import Button from '.';
import { render } from '@testing-library/react';

describe('Button', () => {
  it('Should render correctly', () => {
    const { container } = render(<Button text="button" />);
    expect(container).toMatchSnapshot();
  });
});
