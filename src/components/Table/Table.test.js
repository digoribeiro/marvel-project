import React from 'react';
import { render } from '@testing-library/react';
import Table from '.';

const props = {
  header: ['Nome', 'Descrição', 'Última atualização'],
  columns: 'name.description.modified',
  data: [
    { id: 1011334, name: '3-D Man', description: '', modified: '2014-04-29T14:18:17-0400' },
    {
      id: 1017100,
      name: 'A-Bomb (HAS)',
      description:
        "Rick Jones has been Hulk's best bud since day one,…ses it like a giant bowling ball of destruction! ",
      modified: '2013-09-18T15:54:04-0400'
    },
    {
      id: 1009144,
      name: 'A.I.M.',
      description: 'AIM is a terrorist organization bent on destroying the world.',
      modified: '2013-10-17T14:41:30-0400'
    },
    { id: 1010699, name: 'Aaron Stack', description: '', modified: '1969-12-31T19:00:00-0500' }
  ],
  itemPerPage: 10,
  totalCount: 4
};

describe('Table', () => {
  it('Should render correctly', () => {
    const container = render(<Table {...props} />);
    expect(container).toMatchSnapshot();
  });
});
