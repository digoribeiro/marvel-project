import styled from 'styled-components';
import { colors } from '@styles';

const Styles = styled.div`

  table {
    width: 100%;
    background-color: ${colors.transparent};
    color: ${colors.greyDark};
    border-collapse: collapse;
    border-spacing: 0;
    border-collapse: separate;
    border: 1px solid ${colors.greyLight};
    table-layout: auto;

    tbody {
      tr {
        height: 80px;

        :nth-child(even),
        :hover {
          background-color: ${colors.light};
        }
      }
    }

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid ${colors.greyLight};
      border-right: 1px solid ${colors.greyLight};
      border-width: 0 0 1px;
      padding: 0.5em 0.75em;
      vertical-align: middle;

      :last-child {
        border-right: 0;
      }

      :nth-child(even) {
        width: 60%;
      }

      :nth-child(odd) {
        width: 20%;
      }
    }

    th {
      font-size: 16px;
      text-align: left;
      font-weight: 600;
      border-width: 0 0 2px;
      background: ${colors.greyDark};
      color: ${colors.light};

      :nth-child(even) {
        text-align: center;
      }
    }
  }
  .pagination-status {
    align-items: center;
    display: flex;
    list-style: none;
    font-size: 1rem;
    margin: 20px 0;
  }
  .pagination-status__btn {
    transition: all 0.4s ease;
    border: 0;
    background: ${colors.white};
    color: ${colors.greyDark};
    min-width: 2.5em;
    white-space: nowrap;
    font-size: 1em;
    margin: 0.25rem;
    padding: 0.5em;
    opacity: 1;
  }

  .pagination-status__btn--active {
    background: ${colors.secundary};
    color: ${colors.light};
  }

  .pagination-status__btn:hover {
    background: ${colors.primary};
    color: ${colors.light};
  }

  .pagination-status__btn--disable {
    opacity: 0.5;
  }

@media screen and (max-width: 767px) {
  .pagination-status {
    justify-content: space-between;
  }

  .pagination-status__item {
    display: none;
  }

  .pagination-status__item {
    :first-child,
    :last-child {
      display: block;
    }
  }
`;

export default Styles;
