import React from 'react';
import PropTypes from 'prop-types';
import { TablePagination } from 'react-pagination-table';
import Styles from './Table.styles';

const Table = ({ header, data, columns, totalCount, itemPerPage }) => (
  <Styles>
    <TablePagination
      headers={header}
      data={data}
      columns={columns}
      perPageItemCount={itemPerPage}
      totalCount={totalCount}
      arrayOption={[['size', 'all', ', ']]}
    />
  </Styles>
);

Table.propTypes = {
  header: PropTypes.arrayOf(PropTypes.string).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  columns: PropTypes.string.isRequired,
  totalCount: PropTypes.number.isRequired,
  itemPerPage: PropTypes.number.isRequired
};

export default Table;
