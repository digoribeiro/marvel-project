import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import history from '@root/history';
import App from '@root/App';

render(
  <BrowserRouter history={history}>
    <App />
  </BrowserRouter>,
  global.document.getElementById('root')
);
