const axios = {
  get: jest.fn(() => Promise.resolve({})),
  create: () => axios
};

export default axios;
