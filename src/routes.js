import React from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';

import isAuthenticated from '@services/auth';
import Login from '@pages/Login';
import Home from '@pages/Home';
import Characters from '@pages/Characters';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() ? (
        <>
          <Component {...props} />
        </>
      ) : (
        <Redirect to={{ pathname: '/', state: { from: props.location } }} />
      )
    }
  />
);
function Routes() {
  return (
    <HashRouter>
      <Switch>
        <Route path="/" exact component={Login} />
        <PrivateRoute path="/home" component={Home} />
        <PrivateRoute path="/characters/:id" component={Characters} />
      </Switch>
    </HashRouter>
  );
}

export default Routes;
