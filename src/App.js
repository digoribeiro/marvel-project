import React from 'react';
import Routes from '@root/routes';
import GlobalStyle from '@root/global.styles';

function App() {
  return (
    <>
      <GlobalStyle />
      <div className="container">
        <Routes />
      </div>
    </>
  );
}

export default App;
