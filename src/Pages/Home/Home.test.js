import { Router } from 'react-router-dom';
import mockAxios from 'axios';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import Home from '.';

const mockData = [
  {
    id: 1011334,
    name: '3-D Man',
    description: '',
    modified: '2014-04-29T14:18:17-0400'
  },
  {
    id: 1017100,
    name: 'A-Bomb (HAS)',
    description:
      "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
    modified: '2013-09-18T15:54:04-0400'
  },
  {
    id: 1009144,
    name: 'A.I.M.',
    description: 'AIM is a terrorist organization bent on destroying the world.',
    modified: '2013-10-17T14:41:30-0400'
  }
];

mockAxios.get.mockImplementationOnce(() =>
  Promise.resolve({
    data: {
      data: mockData
    }
  })
);

const mockCredential = {
  privateKey: '323d58c7f9a0ec779f763666286587f132d97556',
  publicKey: '1a78af271c781a0c0fe83bb82edd1135',
  url: '/characters'
};

describe('Home', () => {
  beforeEach(() => {
    jest.mock('react-router-dom', () => ({
      useHistory: () => ({
        push: jest.fn()
      })
    }));
  });

  const history = createMemoryHistory();
  const route = '/Home';
  history.push(route);

  it('Should render correctly', () => {
    const { container } = render(
      <Router history={history}>
        <Home />
      </Router>
    );
    expect(container).toMatchSnapshot();
  });

  it('Should test formatData function', () => {
    const wrapper = mount(
      <Router history={history}>
        <Home getCharacters={mockCredential} />
      </Router>
    );
    expect(mockAxios.get).toBeCalledTimes(1);
  });

  afterEach(() => {
    mockAxios.get.mockReset();
  });
});
