import React, { useEffect, useState } from 'react';
import { Header, Table, MainTitle, Loading, Link } from '@components';
import getCharacters from '@services/getCharacters';
import moment from 'moment';

function Home() {
  const privateKey = global.localStorage.getItem('privateKey');
  const publicKey = global.localStorage.getItem('publicKey');
  const url = '/characters';

  const [characters, setCharacters] = useState([]);
  const [loading, setLoading] = useState(true);

  const Anchor = ({ to, text }) => <Link link={to} text={text} />;

  const formatData = data => {
    const characters = data.map(character => {
      return {
        modified: moment(character.modified).format('MM/DD/YYYY'),
        name: <Anchor to={`characters/${character.id}`} text={character.name} />,
        id: character.id,
        description: character.description
      };
    });
    setCharacters(characters);
  };

  useEffect(() => {
    getCharacters(privateKey, publicKey, url)
      .then(response => {
        formatData(response.data.results);
        setLoading(false);
      })
      .catch(error => setLoading(false));
  }, []);

  const header = ['Nome', 'Descrição', 'Última atualização'];

  return (
    <>
      <Header />
      <div className="container">
        <MainTitle text="Home" />
        {loading ? (
          <Loading />
        ) : (
          <Table
            header={header}
            data={characters}
            totalCount={characters.length}
            columns="name.description.modified"
            itemPerPage={10}
            key={characters.length}
          />
        )}
      </div>
    </>
  );
}

export default Home;
