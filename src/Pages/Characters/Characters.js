import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Profile, Header, MainTitle, Loading, Link } from '@components';

import getCharacters from '@services/getCharacters';

const Characters = () => {
  const { id } = useParams();
  const [character, setCharacter] = useState([]);
  const [comics, setComics] = useState([]);
  const [loading, setLoading] = useState(true);

  const privateKey = global.localStorage.getItem('privateKey');
  const publicKey = global.localStorage.getItem('publicKey');

  const urlCharacter = `/characters/${id}`;
  const urlComic = `/characters/${id}/comics`;
  const imageNotAvail = 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/imageNotAvailable';

  useEffect(() => {
    getCharacters(privateKey, publicKey, urlCharacter)
      .then(response => {
        setCharacter(response.data.results[0]);
      })
      .then(() =>
        getCharacters(privateKey, publicKey, urlComic).then(response => {
          setComics(response.data.results);
          setLoading(false);
        })
      )
      .catch(error => setLoading(false));
  }, []);

  const renderThumbnail = item => {
    const { thumbnail } = item;

    if (thumbnail && thumbnail.path != imageNotAvail) {
      return `${thumbnail.path}.${thumbnail.extension}`;
    }
  };

  const RenderComics = () =>
    comics.map(comic => (
      <Profile
        avatar={renderThumbnail(comic) || imageNotAvail}
        name={comic.title || ''}
        edition={comic.digitalId || ''}
        title={comic.title || ''}
        description={comic.description || ''}
        key={comic.id}
      />
    ));

  const linkStyle = {
    position: ' -webkit-sticky',
    position: 'sticky',
    top: '30px',
    background: '#FFF',
    padding: '10px',
    width: 'max-content'
  };

  return (
    <>
      <Header />
      <MainTitle text="Perfil" />
      {loading ? (
        <Loading />
      ) : (
        [
          <div className="container">
            <Profile
              avatar={renderThumbnail(character) || imageNotAvail}
              name={character.name || ''}
              description={character.description || ''}
            />
            <Link style={linkStyle} link="/home" text="&larr; Voltar" role="button" />
            {comics.length > 0 ? (
              [
                <>
                  <MainTitle text="Fascículos" />
                  <hr />
                  <RenderComics />
                </>
              ]
            ) : (
              <MainTitle text="Nenhum item foi encontrado =(" />
            )}
          </div>
        ]
      )}
    </>
  );
};

export default Characters;
