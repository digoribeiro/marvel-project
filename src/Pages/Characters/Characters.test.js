import React from 'react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import Characters from '.';

describe('Characters', () => {
  beforeEach(() => {
    jest.mock('react-router-dom', () => ({
      useHistory: () => ({
        push: jest.fn()
      })
    }));
  });

  const history = createMemoryHistory();
  const route = '/characters';
  history.push(route);

  it('Should render correctly', () => {
    const { container } = render(
      <Router history={history}>
        <Characters />
      </Router>
    );
    expect(container).toMatchSnapshot();
  });

  it('Should render correctly', () => {
    const { container } = render(
      <Router history={history}>
        <Characters />
      </Router>
    );
    expect(container).toMatchSnapshot();
  });

  it('Should render correctly', () => {
    const { container } = render(
      <Router history={history}>
        <Characters />
      </Router>
    );
    expect(container).toMatchSnapshot();
  });
});
