import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Input, MainTitle, Button, Notification } from '@components';
import getCharacters from '@services/getCharacters';
import Wrapper from './Login.styles';

const Login = () => {
  const [publicKey, setPublicKey] = useState();
  const [privateKey, setPrivateKey] = useState();
  const [messageError, setMessageError] = useState(false);
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  function handleLogin(e) {
    e.preventDefault();
    setLoading(true);

    getCharacters(privateKey, publicKey)
      .then(response => {
        global.localStorage.setItem('publicKey', publicKey);
        global.localStorage.setItem('privateKey', privateKey);
        history.push('/home');
        setLoading(false);
      })
      .catch(() => {
        setMessageError(true);
        setLoading(false);
        global.localStorage.clear();
      });
  }
  return (
    <section className="container">
      <Wrapper>
        <MainTitle text="Login" />
        <form onSubmit={handleLogin} data-testid="form">
          <Input
            label="Public key"
            id="publicKey"
            name="publicKey"
            icon="lock"
            placeholder="Coloque sua Public Key"
            required
            value={publicKey}
            onChange={e => setPublicKey(e.target.value)}
          />
          <Input
            label="Private key"
            id="priviteKey"
            name="priviteKey"
            icon="lock"
            placeholder="Coloque sua Private key"
            required
            value={privateKey}
            onChange={e => setPrivateKey(e.target.value)}
          />
          {messageError && (
            <Notification type="error" text="Por favor verifique suas credenciais" />
          )}
          <Button text="Avançar" icon={loading ? 'spinner fa-spin' : 'arrow-right'} />
        </form>
      </Wrapper>
    </section>
  );
};
export default Login;
