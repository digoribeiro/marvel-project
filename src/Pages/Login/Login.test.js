import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import Login from '.';

describe('Login', () => {
  beforeEach(() => {
    jest.mock('react-router-dom', () => ({
      useHistory: () => ({
        push: jest.fn()
      })
    }));
  });

  const history = createMemoryHistory();
  const route = '/';
  history.push(route);

  it('Should render correctly', () => {
    const { container } = render(
      <Router history={history}>
        <Login />
      </Router>
    );
    expect(container).toMatchSnapshot();
  });

  it('should validate submit', () => {
    const onSubmit = jest.fn();
    const wrapper = mount(
      <Router history={history}>
        <Login handleLogin={jest.fn()} onSubmit={onSubmit} />
      </Router>
    );
    wrapper
      .find('form')
      .at(0)
      .simulate('submit');
    wrapper.update();
    expect(onSubmit).not.toHaveBeenCalled();
  });
});
