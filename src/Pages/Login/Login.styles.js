import styled from 'styled-components';

const Wrapper = styled.div`
  width: 60%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;

  @media (max-width: 767px) {
    width: 80%;
  }
`;

export default Wrapper;
