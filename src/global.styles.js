import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    padding: 0;
    margin: 0;
    outline: 0;
  }

  body {
    font-family: -apple-system, Ubuntu, sans-serif;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.5;
    background: #f0f0f5;
    -webkit-font-smoothing: antialiased;
  }

  -moz-user-input, button, textarea {
    font-family: -apple-system, Ubuntu, sans-serif;
    font-size: 18px;
    font-weight: 400;
  }

  button {
    cursor: point;
  }

  .container {
    flex-grow: 1;
    margin: 0 auto;
    position: relative;
    width: auto;
  }

  @media screen and (min-width: 1024px) {
    .container {
        max-width: 960px;
    }
  }

  @media screen and (max-width: 767px) {
    .container {
        max-width: 95%;
    }
  }

  @media screen and (min-width: 1216px) {
    .container {
        max-width: 1152px;
    }
  }

  @media screen and (min-width: 1408px) {
    .container {
        max-width: 1344px;
    }
  }

  .columns {
    display: flex;
  }

  .columns-wrap {
    flex-wrap: wrap;
  }

  .columns-justify-content {
    justify-content: space-between;
  }

  .column {
    margin-left:  10px;
  }

`;

export default GlobalStyle;
